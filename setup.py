from setuptools import find_packages
from distutils.core import setup
import sys
sys.path.append(r'c:\Users\onselen\Projects\UpFun\MorphoPy\morphopy\main')
import py2exe

try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except ImportError:
    long_description = ''

setup(
    name='morphopy',
    windows=['main/mainapp.py'],
    options={
              "py2exe":{
                    "optimize": 2,
                    "includes": ['panel_prepare.py' ], # List of all the modules you want to import
                    "packages": [] # List of the package you want to make sure that will be imported
               }
        },
    version='0.1',
    description='MorphoPy - Tools for morphological analysis',
    long_description=long_description,
    url='https://github.com/TheFrett/morphopy',
    author='Erik van Onselen',
    author_email='erik.vanonselen@deltares.nl',
    license='MIT',
    packages=find_packages(),
    package_dir={"morphopy": "morphopy"},
    #test_suite='tests',
    python_requires='>=3.9',
    # install_requires=[
    #     'numpy',
    #     'imod',
    #     'psutil',
    #     'rasterio',
    #     'pandas',
    #     'xarray',
    #     'wxPython',
    # ],
    # extras_require={
    #     "dev": ["pytest", "sphinx", "sphinx_rtd_theme"],
    # },
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
    ],
)
