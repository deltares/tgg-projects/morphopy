# MorphoPy

MorphoPy is a GUI program that can be used to combine and analyse bathymetric data

## MorphoPy-Prepare 
Raster preprocessing using Xarray + Dask for processing larger-than-memory datasets
and some Imod-Python rasterio and regrid functions to transform data.

- Loading and converting rasters to the same grid and coordinate reference system
- Determining dt between rasters
- Creating a single DataArray from multi-temporal raster data + export to NetCDF
- Creating mosaic data, providing both the mosaiced data and a raster indicating which 'time' lies on top
- A method to combine and export tiled data (non-overlapping). e.g. multiple AHN tiles 

![MorphoPy GUI](https://gitlab.com/deltares/tgg-projects/morphopy/-/raw/main/docs/screenshot_prepare.PNG)

## MorphoPy-Analysis
Analysis of bathymetry timeseries (not implemented in the GUI yet):

- Computing slope, dz/dt, lowest recorded bathymetry etc.
- Visualisation: 2D, 3D and profile plots
- Interpolation between timesteps (For creating smooth animations)
- Includes a simple detection method for Non Erodable Layers (NELs)

![Profile of bathymetry timeseries](https://gitlab.com/deltares/tgg-projects/morphopy/-/raw/main/docs/screenshot_analysis.png)

![Map of dz/dt](https://gitlab.com/deltares/tgg-projects/morphopy/-/raw/main/docs/screenshot_analysis2.png)
