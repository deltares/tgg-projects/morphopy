import wx
import string


class CharValidator(wx.Validator):
    """Validates data as it is entered into the text controls."""

    # ----------------------------------------------------------------------
    def __init__(self, flag, allow_negative=True, block_entry=False):
        super().__init__()
        self.flag = flag
        self.allow_negative = allow_negative
        self.block_entry = block_entry
        self.Bind(wx.EVT_CHAR, self.OnChar)

    # ----------------------------------------------------------------------
    def Clone(self):
        """Required Validator method"""
        return CharValidator(self.flag, self.allow_negative)

    # ----------------------------------------------------------------------
    def Validate(self, win):
        """Validate the contents of the given text control."""
        textCtrl = self.GetWindow()
        text = textCtrl.GetValue()

        if not len(text) > 0:
            wx.MessageBox("No data entered", "Error")
            textCtrl.SetBackgroundColour("pink")
            textCtrl.SetFocus()
            textCtrl.Refresh()
            return False
        else:
            textCtrl.SetBackgroundColour(
                wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            )
            textCtrl.Refresh()
            return True

    # ----------------------------------------------------------------------
    def TransferToWindow(self):
        return True

    # ----------------------------------------------------------------------
    def TransferFromWindow(self):
        return True

    # ----------------------------------------------------------------------
    def OnChar(self, event):
        keycode = int(event.GetKeyCode())
        if keycode < 256:
            # print keycode
            key = chr(keycode)
            # print key
            if self.flag == "no-alpha" and key in string.ascii_letters:
                return
            if self.flag == "no-digit" and key in string.digits:
                return
            if self.block_entry:
                return
        event.Skip()


class IntegerValidator(wx.Validator):
    """Validates data as it is entered into the text controls."""

    # ----------------------------------------------------------------------
    def __init__(self, allow_negative=True, block_entry=False):
        super().__init__()
        self.allow_negative = allow_negative
        self.block_entry = block_entry
        self.Bind(wx.EVT_CHAR, self.OnChar)

    # ----------------------------------------------------------------------
    def Clone(self):
        """Required Validator method"""
        return IntegerValidator(self.allow_negative)

    # ----------------------------------------------------------------------
    def Validate(self, win):
        """Validate the contents of the given text control."""
        textCtrl = self.GetWindow()
        text = textCtrl.GetValue()

        try:
            if int(text) > 0:
                good_input = True
            elif self.allow_negative:
                good_input = True
            else:
                good_input = False
                message = "Field must contain a positive integer"
        except ValueError:
            good_input = False
            message = "Field must contain an integer"

        if not good_input:
            wx.MessageBox(message, "Error")
            textCtrl.SetBackgroundColour("pink")
            textCtrl.SetFocus()
            textCtrl.Refresh()
            return False
        else:
            textCtrl.SetBackgroundColour(
                wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            )
            textCtrl.Refresh()
            return True

    # ----------------------------------------------------------------------
    def TransferToWindow(self):
        return True

    # ----------------------------------------------------------------------
    def TransferFromWindow(self):
        return True

    # ----------------------------------------------------------------------
    def OnChar(self, event):
        keycode = int(event.GetKeyCode())
        if keycode < 256:
            # print keycode
            key = chr(keycode)
            # print key
            if key in string.ascii_letters:
                return
            if self.block_entry:
                return
        event.Skip()


class FloatRangeValidator(wx.Validator):
    """Validates data as it is entered into the text controls."""

    # ----------------------------------------------------------------------
    def __init__(self, min=0, max=1, block_entry=False):
        super().__init__()
        self.min = min
        self.max = max
        self.block_entry = block_entry
        self.Bind(wx.EVT_CHAR, self.OnChar)

    # ----------------------------------------------------------------------
    def Clone(self):
        """Required Validator method"""
        return FloatRangeValidator(self.min, self.max)

    # ----------------------------------------------------------------------
    def Validate(self, win):
        """Validate the contents of the given text control."""
        textCtrl = self.GetWindow()
        text = textCtrl.GetValue()

        try:
            if float(text) >= self.min and float(text) <= self.max:
                good_input = True
            else:
                good_input = False
                message = f"Value must be between {self.min} and {self.max}"
        except ValueError:
            good_input = False
            message = "Field must contain a number"

        if not good_input:
            wx.MessageBox(message, "Error")
            textCtrl.SetBackgroundColour("pink")
            textCtrl.SetFocus()
            textCtrl.Refresh()
            return False
        else:
            textCtrl.SetBackgroundColour(
                wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)
            )
            textCtrl.Refresh()
            return True

    # ----------------------------------------------------------------------
    def TransferToWindow(self):
        return True

    # ----------------------------------------------------------------------
    def TransferFromWindow(self):
        return True

    # ----------------------------------------------------------------------
    def OnChar(self, event):
        keycode = int(event.GetKeyCode())
        if keycode < 256:
            # print keycode
            key = chr(keycode)
            # print key
            if key in string.ascii_letters:
                return
            if self.block_entry:
                return
        event.Skip()
