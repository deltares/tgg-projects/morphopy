import wx

# Local imports
from panel_prepare import PreparePanel
from panel_analysis import AnalysisPanel
from shared_objects import SharedObjects

version = "2021.9.01"


class App(wx.App):
    def __init__(self):
        super().__init__(clearSigInt=True)
        self.InitFrame()

    def InitFrame(self):
        frame = MainFrame(
            parent=None, title="MorphoPy v" + version, pos=(100, 100), size=(750, 480)
        )
        frame.Show()


class MainFrame(wx.Frame):
    def __init__(self, parent, title, pos, size):
        super().__init__(parent=parent, title=title, pos=pos, size=size)
        self.OnInit()

    def OnInit(self):
        panel_main = wx.Panel(parent=self)
        notebook = wx.Notebook(panel_main)
        tab_prepare = PreparePanel(notebook)
        tab_analysis = AnalysisPanel(notebook)

        notebook.AddPage(tab_prepare, "Prepare")
        notebook.AddPage(tab_analysis, "Analysis")

        sizer = wx.BoxSizer()
        sizer.Add(notebook, 1, wx.EXPAND)
        panel_main.SetSizer(sizer)


if __name__ == "__main__":
    app = App()
    app.MainLoop()
