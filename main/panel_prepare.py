import wx
import wx.lib.agw.multidirdialog as MDD
from pathlib import Path

# Local imports
from validators import CharValidator, IntegerValidator, FloatRangeValidator
from scripts.prepare_rasters import raster_preprocess


class PreparePanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.SetBackgroundColour(wx.Colour(200, 200, 200, 255))
        self.dirname = ""
        self.is_initialised = False
        self.selected_raster_paths = []
        self.selected_raster_names = []
        self.selected_raster_crss = []
        self.selected_raster_mult = []

        # Add raster button
        self.btn_add_raster = wx.Button(parent=self, label="Add")
        self.Bind(wx.EVT_BUTTON, self.OnAddRasters, self.btn_add_raster)

        # Add raster button
        self.btn_rem_raster = wx.Button(parent=self, label="Remove")
        self.Bind(wx.EVT_BUTTON, self.OnRemoveRasters, self.btn_rem_raster)

        # Add raster button
        self.btn_res_raster = wx.Button(parent=self, label="Reset")
        self.Bind(wx.EVT_BUTTON, self.OnResetRasters, self.btn_res_raster)

        # Add output buttons
        self.btn_do_mosaic = wx.Button(parent=self, label="Mosaic")
        self.Bind(wx.EVT_BUTTON, self.OnCreateMosaic, self.btn_do_mosaic)
        self.btn_do_timeseries = wx.Button(parent=self, label="Timeseries")
        self.Bind(wx.EVT_BUTTON, self.OnCreateTimeseries, self.btn_do_timeseries)
        self.btn_reinit = wx.Button(parent=self, label="Re-initialise")
        self.Bind(wx.EVT_BUTTON, self.OnReInit, self.btn_reinit)

        # Add ListBox of selected rasters
        self.list_rasters = wx.ListBox(
            parent=self, choices=self.selected_raster_names, style=wx.LB_EXTENDED
        )

        # Settings objects
        self.setting_time_name = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="year",
            size=(80, 23),
            validator=CharValidator("no-digit"),
        )
        self.setting_time_format = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="%Y",
            size=(80, 23),
            validator=CharValidator("no-digit"),
        )
        self.setting_resolution = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="20",
            size=(80, 23),
            validator=FloatRangeValidator(min=0.0001, max=9999),
        )
        self.setting_export_crs = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="28992",
            size=(80, 23),
            validator=IntegerValidator(allow_negative=False),
        )
        self.setting_minfraction = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="0",
            size=(80, 23),
            validator=FloatRangeValidator(min=0, max=1),
        )

        self.text_time_name = wx.StaticText(parent=self, label="Time axis name:")
        self.text_time_name.SetToolTip(
            "Name to be given to the time axis in the output NetCDF"
        )
        self.text_time_format = wx.StaticText(parent=self, label="Time axis format:")
        self.text_time_format.SetToolTip(
            "Python time format, like e.g. %y%m%d. search online for docs"
        )
        self.text_resolution = wx.StaticText(parent=self, label="Resolution (m):")
        self.text_resolution.SetToolTip(
            "Desired output resolution of the mosaics and timeseries"
        )
        self.text_export_crs = wx.StaticText(parent=self, label="Output CRS:")
        self.text_export_crs.SetToolTip(
            "Ouput rasters coordinate reference system EPSG number"
        )
        self.text_minfraction = wx.StaticText(parent=self, label="Minimum fraction:")
        self.text_minfraction.SetToolTip(
            "Minimum required fractional coverage of the bbox by a raster"
        )

        # BBox inputs
        self.setting_bbox_xmax = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="28000",
            size=(80, 23),
            validator=IntegerValidator(),
        )
        self.setting_bbox_xmax.SetToolTip("x_max")
        self.setting_bbox_xmin = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="8000",
            size=(80, 23),
            validator=IntegerValidator(),
        )
        self.setting_bbox_xmin.SetToolTip("x_min")
        self.setting_bbox_ymax = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="397000",
            size=(80, 23),
            validator=IntegerValidator(),
        )
        self.setting_bbox_ymax.SetToolTip("y_max")
        self.setting_bbox_ymin = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="377000",
            size=(80, 23),
            validator=IntegerValidator(),
        )
        self.setting_bbox_ymin.SetToolTip("y_min")

        # Output widgets
        self.output_area_name = wx.TextCtrl(
            parent=self, id=wx.ID_ANY, value="ws_monding"
        )
        self.output_folder_name = wx.TextCtrl(parent=self, id=wx.ID_ANY, value="")
        self.btn_open_output_folder = wx.Button(parent=self, label="...")
        self.Bind(wx.EVT_BUTTON, self.OnOpenOutputFolder, self.btn_open_output_folder)

        # Load settings from a previous session
        self.LoadPersistence()
        if len(self.selected_raster_names) > 0:
            self.list_rasters.InsertItems(self.selected_raster_names)

        # Object arrangement
        self.CreateLayout()

    def SavePersistence(self):
        attributes_to_save = (
            self.selected_raster_paths,
            self.selected_raster_names,
            self.selected_raster_crss,
            self.selected_raster_mult,
            self.setting_time_name,
        )

    def LoadPersistence(self, panel="prepare"):
        pass

    def CreateLayout(self):
        # Static Boxes
        input_box = wx.StaticBox(
            self,
            id=wx.ID_ANY,
            label="Raster input",
            style=0,
        )
        settings_box = wx.StaticBox(
            self,
            id=wx.ID_ANY,
            label="Settings",
            style=0,
        )
        output_box = wx.StaticBox(
            self,
            id=wx.ID_ANY,
            label="Output",
            style=0,
        )

        # LEFT ------------------------------------------------
        # LEFT components
        self.list_buttons_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.list_buttons_sizer.Add(
            self.btn_add_raster, 0, flag=wx.LEFT | wx.ALIGN_CENTER
        )
        self.list_buttons_sizer.AddSpacer(8)
        self.list_buttons_sizer.Add(
            self.btn_rem_raster, 0, flag=wx.ALIGN_CENTER, border=8
        )
        self.list_buttons_sizer.AddSpacer(8)
        self.list_buttons_sizer.Add(
            self.btn_res_raster, 0, flag=wx.RIGHT | wx.ALIGN_CENTER
        )

        # LEFT main
        self.input_vsizer = wx.StaticBoxSizer(input_box, wx.VERTICAL)
        self.input_vsizer.Add(self.list_rasters, 10, wx.EXPAND)
        self.input_vsizer.AddSpacer(8)
        self.input_vsizer.Add(self.list_buttons_sizer, 0, wx.BOTTOM)

        # RIGHT -----------------------------------------------
        # RIGHT components
        self.right_top = wx.GridSizer(5, 2, 5, 10)
        self.right_top.AddMany(
            [
                (self.text_time_name),
                (self.setting_time_name, 1, wx.EXPAND),
                (self.text_time_format),
                (self.setting_time_format, 1, wx.EXPAND),
                (self.text_resolution),
                (self.setting_resolution, 1, wx.EXPAND),
                (self.text_export_crs),
                (self.setting_export_crs, 1, wx.EXPAND),
                (self.text_minfraction),
                (self.setting_minfraction, 1, wx.EXPAND),
            ]
        )

        self.right_bottom = wx.GridSizer(4, 3, 2, 2)
        self.right_bottom.AddMany(
            [
                (wx.StaticText(self), wx.EXPAND),
                (
                    wx.StaticText(self, label="\nBounding Box"),
                    wx.EXPAND | wx.ALIGN_CENTER | wx.ALIGN_BOTTOM,
                ),
                (wx.StaticText(self), wx.EXPAND),
                (wx.StaticText(self), wx.EXPAND),
                (self.setting_bbox_ymax, 0, wx.EXPAND),
                (wx.StaticText(self), wx.EXPAND),
                (self.setting_bbox_xmin, 0, wx.EXPAND),
                (wx.StaticText(self), wx.EXPAND),
                (self.setting_bbox_xmax, 0, wx.EXPAND),
                (wx.StaticText(self), wx.EXPAND),
                (self.setting_bbox_ymin, 0, wx.EXPAND),
                (wx.StaticText(self), wx.EXPAND),
            ]
        )

        self.settings_vsizer = wx.StaticBoxSizer(settings_box, wx.VERTICAL)
        self.settings_vsizer.Add(self.right_top, 1, wx.EXPAND)
        self.settings_vsizer.Add(self.right_bottom, 1, wx.EXPAND | wx.ALIGN_TOP)

        self.output_sizer = wx.GridBagSizer(5, 5)
        self.output_sizer.Add(
            wx.StaticText(self, label="Area name:"),
            pos=(0, 0),
            span=(1, 1),
            flag=wx.EXPAND,
        )
        self.output_sizer.Add(
            wx.StaticText(self, label="Output folder:"),
            pos=(1, 0),
            span=(1, 1),
            flag=wx.EXPAND,
        )
        self.output_sizer.Add(
            self.output_area_name, pos=(0, 1), span=(1, 6), flag=wx.EXPAND
        )
        self.output_sizer.Add(
            self.output_folder_name, pos=(1, 1), span=(1, 5), flag=wx.EXPAND
        )
        self.output_sizer.Add(
            self.btn_do_mosaic, pos=(2, 1), span=(1, 1), flag=wx.EXPAND
        )
        self.output_sizer.Add(
            self.btn_do_timeseries, pos=(2, 2), span=(1, 1), flag=wx.EXPAND
        )
        self.output_sizer.Add(self.btn_reinit, pos=(2, 3), span=(1, 1), flag=wx.EXPAND)
        self.output_sizer.Add(
            self.btn_open_output_folder, pos=(1, 6), span=(1, 1), flag=wx.EXPAND
        )

        self.output_sizer.SetFlexibleDirection(wx.HORIZONTAL)

        self.right_output_controls = wx.StaticBoxSizer(output_box, wx.VERTICAL)
        self.right_output_controls.Add(self.output_sizer, wx.EXPAND)

        # RIGHT main
        self.right_main_vsizer = wx.BoxSizer(wx.VERTICAL)
        self.right_main_vsizer.Add(self.settings_vsizer, 1, wx.EXPAND)
        self.right_main_vsizer.Add(self.right_output_controls, 1, wx.EXPAND)

        # MAIN ------------------------------------------------
        # MAIN sizer
        self.mainSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.mainSizer.Add(self.input_vsizer, 1, wx.EXPAND)
        self.mainSizer.AddSpacer(8)
        self.mainSizer.Add(self.right_main_vsizer, 1, wx.EXPAND)
        self.SetSizer(self.mainSizer)

    def OnAddRasters(self, e):
        with AskRasterDialog(parent=self, title="Add rasters") as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                pass
            else:
                if len(dlg.selected_rasters) > 0:
                    self.selected_raster_paths += dlg.selected_rasters
                    self.selected_raster_crss += dlg.selected_crss
                    self.selected_raster_mult += dlg.selected_mult
                    # Display name:
                    new_names = [
                        f.stem + " * " + str(m) + "; CRS = EPSG" + c
                        for f, c, m in zip(
                            dlg.selected_rasters, dlg.selected_crss, dlg.selected_mult
                        )
                    ]
                    self.selected_raster_names += new_names

                    self.list_rasters.InsertItems(new_names, pos=0)
                else:
                    pass

    def OnRemoveRasters(self, e):
        items_to_remove = self.list_rasters.GetSelections()
        item_strings_to_remove = [
            self.list_rasters.GetString(i) for i in items_to_remove
        ]

        for item_string_to_remove in item_strings_to_remove:
            self.list_rasters.Delete(
                self.list_rasters.FindString(item_string_to_remove)
            )

        # Remove data from internal lists
        self.selected_raster_paths = [
            p
            for i, p in enumerate(self.selected_raster_paths)
            if not i in items_to_remove
        ]
        self.selected_raster_names = [
            p
            for i, p in enumerate(self.selected_raster_names)
            if not i in items_to_remove
        ]
        self.selected_raster_crss = [
            p
            for i, p in enumerate(self.selected_raster_crss)
            if not i in items_to_remove
        ]
        self.selected_raster_mult = [
            p
            for i, p in enumerate(self.selected_raster_mult)
            if not i in items_to_remove
        ]

    def OnResetRasters(self, e):
        self.list_rasters.Clear()
        self.list_rasters.Update()
        self.selected_raster_paths = []
        self.selected_raster_names = []
        self.selected_raster_crss = []
        self.selected_raster_mult = []

    def OnOpenOutputFolder(self, e):
        """Open a folder"""
        dlg = wx.DirDialog(
            parent=self,
            message="Choose an output folder",
            defaultPath="",
            style=wx.DD_DIR_MUST_EXIST,
        )
        if dlg.ShowModal() == wx.ID_OK:
            self.output_path = dlg.GetPath()
            self.output_folder_name.SetValue(self.output_path)
        dlg.Destroy()

    def OnCreateMosaic(self, e):
        if self.CheckSettings() and self.CheckOutputPath():
            with wx.ProgressDialog(
                "Creating mosaic",
                "Loading source rasters",
                maximum=len(self.selected_raster_paths),
                style=wx.PD_APP_MODAL
                | wx.PD_AUTO_HIDE
                | wx.PD_CAN_ABORT
                | wx.PD_ELAPSED_TIME
                | wx.PD_ESTIMATED_TIME,
            ) as prgd:
                if not self.is_initialised:
                    self.InitPreprocess()
                    self.preprocess.load(
                        float(self.setting_minfraction.Value),
                        progressbar=prgd,
                        chunk=False,
                    )
                    self.is_initialised = True
                    self.UpdateValidators()
                if self.is_initialised:
                    self.preprocess.mosaic(progressbar=prgd)
                    prgd.Update(0, newmsg="Saving results")
                    prgd.Pulse()
                    self.preprocess.mosaic_to_tif(
                        mosaic_file=Path(self.output_folder_name.Value).joinpath(
                            self.output_area_name.Value + "_mosaic.tiff"
                        ),
                        latest_file=Path(self.output_folder_name.Value).joinpath(
                            self.output_area_name.Value + "_latest.tiff"
                        ),
                    )

    def OnCreateTimeseries(self, e):
        if self.CheckSettings() and self.CheckOutputPath():
            with wx.ProgressDialog(
                "Creating timeseries",
                "Loading source rasters",
                maximum=len(self.selected_raster_paths),
                style=wx.PD_APP_MODAL
                | wx.PD_AUTO_HIDE
                | wx.PD_CAN_ABORT
                | wx.PD_ELAPSED_TIME
                | wx.PD_ESTIMATED_TIME,
            ) as prgd:
                if not self.is_initialised:
                    self.InitPreprocess()
                    self.preprocess.load(
                        float(self.setting_minfraction.Value),
                        progressbar=prgd,
                        chunk=False,
                    )
                    self.is_initialised = True
                    self.UpdateValidators()
                if self.is_initialised:
                    prgd.Update(0, newmsg="Saving results")
                    prgd.Pulse()
                    self.preprocess.timeseries_to_nc(
                        file=Path(self.output_folder_name.Value).joinpath(
                            self.output_area_name.Value + "_timeseries.nc"
                        )
                    )

    def OnReInit(self, e):
        self.is_initialised = False
        self.UpdateValidators()

    def CheckSettings(self):
        return all(
            [
                self.setting_time_name.GetValidator().Validate(self.setting_time_name),
                self.setting_time_format.GetValidator().Validate(
                    self.setting_time_format
                ),
                self.setting_resolution.GetValidator().Validate(
                    self.setting_resolution
                ),
                self.setting_export_crs.GetValidator().Validate(
                    self.setting_export_crs
                ),
                self.setting_minfraction.GetValidator().Validate(
                    self.setting_minfraction
                ),
                self.setting_bbox_xmin.GetValidator().Validate(self.setting_bbox_xmin),
                self.setting_bbox_xmax.GetValidator().Validate(self.setting_bbox_xmax),
                self.setting_bbox_ymin.GetValidator().Validate(self.setting_bbox_ymin),
                self.setting_bbox_ymax.GetValidator().Validate(self.setting_bbox_ymax),
            ]
        )

    def CheckOutputPath(self):
        return Path(self.output_folder_name.GetValue()).is_dir()

    def UpdateValidators(self):
        self.setting_time_name.GetValidator().block_entry = self.is_initialised
        self.setting_time_format.GetValidator().block_entry = self.is_initialised
        self.setting_resolution.GetValidator().block_entry = self.is_initialised
        self.setting_export_crs.GetValidator().block_entry = self.is_initialised
        self.setting_minfraction.GetValidator().block_entry = self.is_initialised
        self.setting_bbox_xmax.GetValidator().block_entry = self.is_initialised
        self.setting_bbox_xmin.GetValidator().block_entry = self.is_initialised
        self.setting_bbox_ymax.GetValidator().block_entry = self.is_initialised
        self.setting_bbox_ymin.GetValidator().block_entry = self.is_initialised

    def InitPreprocess(self):
        self.preprocess = raster_preprocess(
            self.selected_raster_paths,
            self.selected_raster_crss,
            self.selected_raster_mult,
            time_format=self.setting_time_format.Value,
            time_axis_name=self.setting_time_name.Value,
            desired_res=float(self.setting_resolution.Value),
            destination_crs=self.setting_export_crs.Value,
            bbox=[
                float(self.setting_bbox_xmin.Value),
                float(self.setting_bbox_xmax.Value),
                float(self.setting_bbox_ymin.Value),
                float(self.setting_bbox_ymax.Value),
            ],
        )


class AskRasterDialog(wx.Dialog):
    def __init__(self, parent, title):
        super().__init__(
            parent=parent, title=title, style=wx.DEFAULT_DIALOG_STYLE, size=(400, 150)
        )
        self.dirname = ""
        self.selected_rasters = []
        self.selected_crss = []
        self.selected_mult = []

        # Add button for opening other raster files
        self.btn_add_other = wx.Button(self, label="Add other formats")
        self.Bind(wx.EVT_BUTTON, self.OnAddOtherRasters, self.btn_add_other)

        # Add button for opening ESRI raster files (folders)
        self.btn_add_esri = wx.Button(self, label="Add ESRI format")
        self.Bind(wx.EVT_BUTTON, self.OnAddESRIRasters, self.btn_add_esri)

        # Add field to enter CRS EPSG and raster multiplication
        self.crs_hint = wx.StaticText(parent=self, label="CRS EPSG:")
        self.multiply_hint = wx.StaticText(parent=self, label="Multiply by:")
        self.crs_entry = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="28992",
            size=(80, 23),
            validator=IntegerValidator(allow_negative=False),
        )
        self.multiply_entry = wx.TextCtrl(
            parent=self,
            id=wx.ID_ANY,
            value="1",
            size=(40, 23),
            validator=IntegerValidator(),
        )

        # Object arrangement
        self.CreateLayout()

    def CreateLayout(self):
        self.settingsBox = wx.StaticBox(
            parent=self, id=wx.ID_ANY, label="Raster properties", style=0
        )

        # Upper box, divided horizontally
        self.upper_sizer = wx.StaticBoxSizer(self.settingsBox, wx.HORIZONTAL)
        self.upper_sizer.Add(self.crs_hint, 1, flag=wx.LEFT | wx.CENTER, border=5)
        self.upper_sizer.Add(self.crs_entry, 1, flag=wx.ALIGN_LEFT | wx.CENTER)
        self.upper_sizer.AddSpacer(16)
        self.upper_sizer.Add(self.multiply_hint, 1, flag=wx.ALIGN_LEFT | wx.CENTER)
        self.upper_sizer.Add(
            self.multiply_entry, 1, flag=wx.RIGHT | wx.CENTER, border=5
        )

        # Lower box, with the two buttons
        self.lower_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.lower_sizer.Add(self.btn_add_other, 1, wx.EXPAND)
        self.lower_sizer.Add(self.btn_add_esri, 1, wx.EXPAND)

        # MAIN sizer
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(self.upper_sizer, 1, wx.EXPAND)
        self.mainSizer.Add(self.lower_sizer, 1, wx.EXPAND)
        self.SetSizer(self.mainSizer)

    def OnAddOtherRasters(self, e):
        """Open a file"""
        if self.crs_entry.GetValidator().Validate(
            self.crs_entry
        ) and self.multiply_entry.GetValidator().Validate(self.multiply_entry):
            # self.selected_rasters = []
            # self.selected_crss = []
            # self.selected_mult = []
            dlg = wx.FileDialog(
                parent=self,
                message="Choose a raster file",
                defaultDir=self.dirname,
                defaultFile="",
                wildcard="*.*",
                style=wx.FD_OPEN | wx.FD_MULTIPLE,
            )
            if dlg.ShowModal() == wx.ID_OK:
                self.selected_rasters += [Path(f) for f in dlg.GetPaths()]
                self.selected_crss += [
                    self.crs_entry.GetValue() for f in self.selected_rasters
                ]
                self.selected_mult += [
                    int(self.multiply_entry.GetValue()) for f in self.selected_rasters
                ]
            dlg.Destroy()
            if len(self.selected_rasters) > 0:
                self.Destroy()
        else:
            pass

    # def OnAddESRIRasters(self, e):
    #     """ Open a folder"""
    #     if self.crs_entry.GetValidator().Validate(self.crs_entry) and self.multiply_entry.GetValidator().Validate(self.multiply_entry):
    #         dlg = wx.DirDialog(parent = self,
    #                         message = "Choose an ESRI raster folder",
    #                         defaultPath = "",
    #                         style = wx.DD_MULTIPLE|wx.DD_DIR_MUST_EXIST
    #                         )
    #         if dlg.ShowModal() == wx.ID_OK:
    #             self.selected_rasters += [Path(p) for p in dlg.GetPaths()]
    #             self.selected_crss += [self.crs_entry.GetValue() for f in self.selected_rasters]
    #             self.selected_mult += [int(self.multiply_entry.GetValue()) for f in self.selected_rasters]
    #         dlg.Destroy()
    #         if len(self.selected_rasters) > 0:
    #             self.Destroy()
    #     else:
    #         pass

    def OnAddESRIRasters(self, e):
        """Open a folder"""
        if self.crs_entry.GetValidator().Validate(
            self.crs_entry
        ) and self.multiply_entry.GetValidator().Validate(self.multiply_entry):
            dlg = MDD.MultiDirDialog(
                parent=self,
                title="Choose an ESRI raster folder",
                defaultPath="",
                agwStyle=MDD.DD_MULTIPLE | MDD.DD_DIR_MUST_EXIST,
            )
            if dlg.ShowModal() == wx.ID_OK:
                self.selected_rasters += [
                    Path(q[0].strip("(") + q[1])
                    for q in [p.split(" ")[-1].split(")") for p in dlg.GetPaths()]
                ]
                self.selected_crss += [
                    self.crs_entry.GetValue() for f in self.selected_rasters
                ]
                self.selected_mult += [
                    int(self.multiply_entry.GetValue()) for f in self.selected_rasters
                ]
            dlg.Destroy()
            if len(self.selected_rasters) > 0:
                self.Destroy()
        else:
            pass
