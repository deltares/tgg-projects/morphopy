# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 12:12:28 2020

@author: onselen
"""

import xarray as xr
import geopandas as gpd
import pandas as pd
import imod
from datetime import datetime
import dask.array as darr
from pathlib import Path
import numpy as np
from tqdm import tqdm
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from mpl_toolkits import mplot3d
import cmocean
import contextily as ctx
from rasterio.crs import CRS
import numba
import scipy.stats as st
from itertools import product
from math import atan2, degrees

windlabels = [
    "NO",
    "O",
    "ZO",
    "Z",
    "ZW",
    "W",
    "NW",
    "N",
]  # Starts with Northeast, ends with North.
winddirs = np.array([22.5, 67.5, 112.5, 157.5, 202.5, 247.5, 292.5, 337.5])


class raster_morphology:
    def __init__(
        self,
        path_to_nc,
        path_to_mosaic,
        timeaxis=0,
        chunk=True,
        chunksize="auto",
        crs=28992,
    ):
        self.ta = timeaxis
        self.crs = f"epsg:{str(crs)}"

        if chunk:
            self.data = xr.open_dataarray(
                path_to_nc, chunks={"year": 5, "x": "auto", "y": "auto"}
            )
            self.chunks = tuple([c[0] for c in self.data.chunks])
            self.mosaic = (
                xr.open_rasterio(path_to_mosaic, chunks=self.chunks)
                .squeeze()
                .drop_vars("band")
            )
        else:
            self.data = xr.open_dataarray(path_to_nc)
            self.chunks = None
            self.mosaic = xr.open_rasterio(path_to_mosaic)

        try:
            self.res = np.float32(self.data.res[0])
        except AttributeError:
            self.res = np.float32(st.mode(np.diff(self.data["x"]))[0][0])
        self.nx = np.int64(self.data.sizes["x"])
        self.ny = np.int64(self.data.sizes["y"])
        self.tname = self.data.dims[timeaxis]

        # Potential products
        self.interp_da = None
        self.z_max_diff = None
        self.z_min = None
        self.min_current = None
        self.dzdt = None
        self.dzdt_max = None
        self.dzdt_mean = None
        self.slope1 = None
        self.slope2 = None
        self.potential_area_z = None

        # Create data map for accesing options. Format: [variable, title, single/multiple figures, colormap, associated plotfunc]
        self.data_map = {
            "z": [
                self.data,
                "Bathymetrie (m)",
                "multiple",
                cmocean.cm.delta,
                self._plot_bathymetry,
            ],
            "z_interp": [
                self.interp_da,
                "Bathymetrie (m)",
                "multiple",
                cmocean.cm.delta,
                self._plot_bathymetry,
            ],
            "max_diff": [
                self.z_max_diff,
                "Maximaal diepteverschil (m)",
                "single",
                matplotlib.cm.PuOr_r,
                self._plot_difference,
            ],
            "min": [
                self.z_min,
                "Minimum bathymetrie (m)",
                "single",
                cmocean.cm.delta,
                self._plot_bathymetry,
            ],
            "min_current": [
                self.min_current,
                "Verschil minimum en laatst gemeten diepte (m)",
                "single",
                matplotlib.cm.PuOr_r,
                self._plot_difference,
            ],
            "dzdt": [
                self.dzdt,
                "Verticale snelheid (m/dag)",
                "multiple",
                cmocean.cm.balance,
                self._plot_difference,
            ],
            "dzdt_max": [
                self.dzdt_max,
                "Maximale verticale snelheid (m/dag)",
                "single",
                cmocean.cm.balance,
                self._plot_difference,
            ],
            "dzdt_mean": [
                self.dzdt_mean,
                "Gemiddelde verticale snelheid laatste 10 jaar (m/dag)",
                "single",
                cmocean.cm.balance,
                self._plot_difference,
            ],
            "slope1": [
                self.slope1,
                "Helling (\xb0)",
                "single",
                cmocean.cm.haline,
                self._plot_difference,
            ],
            "slope2": [
                self.slope2,
                "Hellingsverandering (\xb0/m)",
                "single",
                cmocean.cm.haline,
                self._plot_difference,
            ],
            "nel_z": [
                self.potential_area_z,
                "Mogelijk actieve erosie-resistente lagen (top in m)",
                "single",
                cmocean.cm.delta,
                self._plot_bathymetry,
            ],
        }

    def minimum(self):
        self.z_min = self.data.min(axis=self.ta)

        self.z_min.attrs = self.data.attrs
        self.z_min.attrs["descriptions"] = "minimum_bathymetry"

        self.data_map["min"][0] = self.z_min

    def greatest_diff(self):
        gdiff = np.abs(self.data.min(axis=self.ta) - self.data.max(axis=self.ta))
        self.z_max_diff = gdiff.where((gdiff > -100) & (gdiff < 100), other=np.nan)

        self.z_max_diff.attrs = self.data.attrs
        self.z_max_diff.attrs["descriptions"] = "maximum_difference_over_period"

        self.data_map["max_diff"][0] = self.z_max_diff

    def current_diff_from_deepest(self):
        self.min_current = self.mosaic - self.z_min

        self.min_current.attrs = self.data.attrs
        self.min_current.attrs["descriptions"] = "difference_now_minimum"

        self.data_map["min_current"][0] = self.min_current

    def diff(self, tax=0, xax=2, yax=1):

        # @numba.guvectorize(
        #                     [(numba.float32[:, :, :], numba.int64, numba.int64, numba.float64, numba.boolean, numba.float32[:, :])],
        #                     '(t, n, m),(),(),(),() -> (n, m)',
        #                 )
        # def compute_dzdt(nparr, ny, nx, res, to_deg, out):

        self.interpolate(tax=tax, xax=xax, yax=yax)
        dt = (
            self.interp_da[self.tname].diff(dim=self.tname)
            / np.timedelta64(1, "h")
            / 24
        )
        self.dzdt = self.interp_da.diff(self.tname) * (1 / dt)
        self.dzdt_max = self.dzdt.max(axis=self.ta)
        self.dzdt_max = self.dzdt_max.where(
            (self.dzdt_max > -50) & (self.dzdt_max < 50), other=np.nan
        )
        self.dzdt_mean = self.dzdt[len(self.dzdt) - 10 :, :, :].mean(axis=self.ta)
        self.dzdt_mean = self.dzdt_mean.where(
            (self.dzdt_mean > -50) & (self.dzdt_mean < 50), other=np.nan
        )

        self.dzdt.attrs = self.data.attrs
        self.dzdt.attrs["descriptions"] = "dz/dt"
        self.dzdt_max.attrs = self.data.attrs
        self.dzdt_max.attrs["descriptions"] = "dz/dt_maximum"
        self.dzdt_mean.attrs = self.data.attrs
        self.dzdt_mean.attrs["descriptions"] = "dz/dt_last_10yrs"

        self.data_map["dzdt"][0] = self.dzdt
        self.data_map["dzdt_max"][0] = self.dzdt_max
        self.data_map["dzdt_mean"][0] = self.dzdt_mean

    def slope(self, xax=1, yax=0):
        @numba.guvectorize(
            [
                (
                    numba.float32[:, :],
                    numba.int64,
                    numba.int64,
                    numba.float32,
                    numba.boolean,
                    numba.float32[:, :],
                )
            ],
            "(n, m),(),(),(),() -> (n, m)",
            nopython=True,
        )
        def compute_slope(nparr, ny, nx, res, to_deg, out):
            slope = np.zeros_like(nparr, dtype=np.float32)
            for yi in range(ny - 1):
                for xi in range(nx - 1):
                    if yi > 0 and xi > 0 and yi < ny - 1 and xi < nx - 1:
                        xslope = np.abs(nparr[yi, xi + 1] - nparr[yi, xi - 1]) / (
                            2 * res
                        )
                        yslope = np.abs(nparr[yi + 1, xi] - nparr[yi - 1, xi]) / (
                            2 * res
                        )
                    elif yi == 0 and xi == 0:
                        xslope = np.abs(nparr[yi, xi + 1] - nparr[yi, xi]) / (res)
                        yslope = np.abs(nparr[yi + 1, xi] - nparr[yi, xi]) / (res)
                    elif yi == 0 and xi > 0 and yi < ny - 1 and xi < nx - 1:
                        xslope = np.abs(nparr[yi, xi + 1] - nparr[yi, xi - 1]) / (
                            2 * res
                        )
                        yslope = np.abs(nparr[yi + 1, xi] - nparr[yi, xi]) / (res)
                    elif yi > 0 and xi == 0 and yi < -1 and xi < nx - 1:
                        xslope = np.abs(nparr[yi, xi + 1] - nparr[yi, xi]) / (res)
                        yslope = np.abs(nparr[yi + 1, xi] - nparr[yi - 1, xi]) / (
                            2 * res
                        )
                    elif yi > 0 and xi > 0 and yi == ny - 1 and xi < nx - 1:
                        xslope = np.abs(nparr[yi, xi + 1] - nparr[yi, xi - 1]) / (
                            2 * res
                        )
                        yslope = np.abs(nparr[yi, xi] - nparr[yi - 1, xi]) / (res)
                    elif yi > 0 and xi > 0 and yi < ny - 1 and xi == nx - 1:
                        xslope = np.abs(nparr[yi, xi] - nparr[yi, xi - 1]) / (res)
                        yslope = np.abs(nparr[yi + 1, xi] - nparr[yi - 1, xi]) / (
                            2 * res
                        )
                    elif yi == ny - 1 and xi == nx - 1:
                        xslope = np.abs(nparr[yi, xi - 1] - nparr[yi, xi]) / (res)
                        yslope = np.abs(nparr[yi - 1, xi] - nparr[yi, xi]) / (res)
                    else:
                        xslope = 0
                        yslope = 0

                    # Convert to degrees
                    slope[yi, xi] = np.arctan(np.sqrt(xslope ** 2 + yslope ** 2))
                    if to_deg:
                        slope[yi, xi] *= 180 / np.pi
            out[:, :] = slope

        # TODO: als dask arrays processen?
        # First derivative
        self.slope1 = xr.apply_ufunc(
            compute_slope,
            self.mosaic.compute(),
            self.ny,
            self.nx,
            self.res,
            True,
            dask="allowed",
            output_dtypes=np.float32,
        ).chunk(chunks=self.chunks[-2:])
        self.slope1.attrs = self.data.attrs
        self.slope1.attrs["descriptions"] = "slope"
        self.data_map["slope1"][0] = self.slope1

        # Second derivative
        self.slope2 = xr.apply_ufunc(
            compute_slope,
            self.slope1.compute(),
            self.ny,
            self.nx,
            self.res,
            False,
            dask="allowed",
            output_dtypes=np.float32,
        ).chunk(chunks=self.chunks[-2:])
        self.slope2.attrs = self.data.attrs
        self.slope2.attrs["descriptions"] = "slope"
        self.data_map["slope2"][0] = self.slope2

    def potential_NEL_area(self):
        cond1 = self.min_current < 0.5
        cond2 = self.mosaic < -8
        cond3 = (self.slope1 < 1) | (self.slope1 > 8)
        cond4 = (self.dzdt_mean < 0) & (self.dzdt_mean > -0.002)
        cond5 = (self.slope2 < 0.1) | (self.slope2 > 0.4)

        # The product of conditions indicates cells where all conditions are True
        self.potential_area_mask = cond1 * cond2 * cond3 * cond4 * cond5
        self.potential_area = self.potential_area_mask.where(
            self.potential_area_mask == 1, other=np.nan
        )

        self.potential_area_z = (
            self.interp_da[-1, :, :] * self.potential_area_mask
        ).where(self.potential_area_mask == 1, other=np.nan)

        # self.potential_area_z.plot.imshow()
        # plt.show()

        self.potential_area_z.attrs = self.data.attrs
        self.potential_area_z.attrs["descriptions"] = "potential_NEL"

        self.data_map["nel_z"][0] = self.potential_area_z

    def interpolate(self, tax=0, xax=2, yax=1, refine_extra=1):
        @numba.njit
        def _interpolate(nparr, taxis, ny, nx):
            for yi in range(ny):
                for xi in range(nx):
                    col = nparr[:, yi, xi]
                    finite = np.isfinite(col)
                    if len(taxis[finite]) > 2:
                        col_interp = np.interp(taxis, taxis[finite], col[finite])
                        col_interp[: np.argmax(finite)] = np.nan
                        nparr[:, yi, xi] = col_interp
                    else:
                        nparr[:, yi, xi] = np.nan
            return nparr

        dt = (
            self.data[self.tname].diff(dim=self.tname).values
            / np.timedelta64(1, "h")
            / 24
        )
        yeardiffs = np.int32(np.round(dt / 365)) * refine_extra
        yeardiffs_cumsum = np.hstack([np.array([0]), np.cumsum(yeardiffs)])
        nt = np.sum(yeardiffs) + 1

        nparr = np.float32(self.data.values)
        # nparr = darr.from_array(self.data, chunks=self.chunks)

        insert_temp = np.empty_like(nparr[0, :, :])
        insert_temp[:, :] = np.nan

        # Allocate new array
        newarr = np.empty([nt, self.ny, self.nx], dtype=np.float32)
        newarr[:, :] = np.nan
        newarr[0, :, :] = nparr[0, :, :]

        icsum = 0
        for i in range(nt):
            if i == yeardiffs_cumsum[icsum]:
                newarr[i, :, :] = nparr[icsum, :, :]
                icsum += 1
            else:
                newarr[i, :, :] = insert_temp

        taxis = np.arange(np.float64(0), np.float64(nt), dtype=np.float64)
        print(
            f"Interpolating grid to {refine_extra} surfaces per year, this may take some time"
        )
        interparr = _interpolate(newarr, taxis, self.ny, self.nx)

        years = pd.date_range(
            self.data[self.tname].values[0] - np.timedelta64(365, "D"),
            self.data[self.tname].values[-1],
            periods=nt,
        )

        self.interp_da = xr.DataArray(
            data=interparr,
            coords=[years, self.data.y, self.data.x],
            dims=[self.tname, "y", "x"],
        )

        # update datamap
        self.data_map["z_interp"][0] = self.interp_da

    def mincust(self, da):
        if da.size != 0:
            da_min = da.min(axis=self.ta)
        else:
            da_min = da
        return da_min

    def shape_to_samplepoints(self, shapefile, step, id_field="id"):
        shape = gpd.read_file(shapefile)
        shape = shape.to_crs(self.crs)

        self.x_samples = []
        self.y_samples = []
        self.id_samples = []
        self.rlabel = []
        self.llabel = []

        for i, row in shape.iterrows():
            geometry = row["geometry"]
            x_segments = np.empty(0)
            y_segments = np.empty(0)
            for i in range(len(geometry.coords) - 1):
                x_minmax = np.array([geometry.coords[i][0], geometry.coords[i + 1][0]])
                y_minmax = np.array([geometry.coords[i][1], geometry.coords[i + 1][1]])

                amount_steps = (
                    np.sqrt(
                        (x_minmax[0] - x_minmax[1]) ** 2
                        + (y_minmax[0] - y_minmax[1]) ** 2
                    )
                    / step
                )
                x_step = (x_minmax[1] - x_minmax[0]) / amount_steps
                y_step = (y_minmax[1] - y_minmax[0]) / amount_steps

                if x_minmax[0] != x_minmax[1]:
                    x_segments = np.hstack(
                        [
                            x_segments,
                            np.arange(
                                x_minmax[0], x_minmax[1], x_step, dtype=np.float32
                            ),
                        ]
                    )
                else:
                    x_segments = np.hstack(
                        [
                            x_segments,
                            np.full(
                                int(np.ceil(amount_steps)),
                                x_minmax[0],
                                dtype=np.float32,
                            ),
                        ]
                    )
                if y_minmax[0] != y_minmax[1]:
                    y_segments = np.hstack(
                        [
                            y_segments,
                            np.arange(
                                y_minmax[0], y_minmax[1], y_step, dtype=np.float32
                            ),
                        ]
                    )
                else:
                    y_segments = np.hstack(
                        [
                            y_segments,
                            np.full(
                                int(np.ceil(amount_steps)),
                                y_minmax[0],
                                dtype=np.float32,
                            ),
                        ]
                    )

            _, label_l = self.wind_label(
                geometry.xy[0][0],
                geometry.xy[1][0],
                geometry.xy[0][1],
                geometry.xy[1][1],
            )

            _, label_r = self.wind_label(
                geometry.xy[0][1],
                geometry.xy[1][1],
                geometry.xy[0][0],
                geometry.xy[1][0],
            )

            self.rlabel.append(label_r)
            self.llabel.append(label_l)
            self.x_samples.append((xr.DataArray(x_segments)))
            self.y_samples.append((xr.DataArray(y_segments)))
            self.id_samples.append(row[id_field])

    def plot_2d(
        self,
        what,
        path,
        titles=None,
        basemap=False,
        grid=False,
        shapefiles=None,
        shapefile_names=None,
        shapefile_widths=None,
        shapefile_styles=None,
        shapefile_colors=None,
        shapefile_labels=None,
        tick_interval=None,
        minmax=None,
        anim=False,
        anim_fps=10,
    ):

        # Check and add shapefile variables
        if type(shapefiles) == list:
            shapes_to_plot = [gpd.read_file(p) for p in shapefiles]
            num_of_shapes = len(shapefiles)
            if shapefile_names == None or len(shapefile_names) != num_of_shapes:
                shapefile_names = [f"Shapefile {n+1}" for n in range(num_of_shapes)]
            if shapefile_widths == None or len(shapefile_names) != num_of_shapes:
                shapefile_widths = [1 for n in range(num_of_shapes)]
            if shapefile_styles == None or len(shapefile_names) != num_of_shapes:
                shapefile_styles = ["-" for n in range(num_of_shapes)]
            if shapefile_colors == None or len(shapefile_names) != num_of_shapes:
                shapefile_colors = ["k" for n in range(num_of_shapes)]

        options = self.data_map[what]
        da = options[0]
        title = options[1]
        single_or_multi = options[2]
        cmap = options[3]
        plotfunc = options[4]

        if single_or_multi == "multiple":
            titles = da[self.tname].values.astype("datetime64[Y]").astype(int) + 1970
            titles = [
                title + " " + t.split("-")[0] + "-" + t.split("-")[1]
                for t in np.datetime_as_string(da[self.tname].values)
            ]
            if anim:
                path = [path.joinpath(what + "_animation.mp4")]
                list_of_images = []
            else:
                path = [path.joinpath(str(t) + "_" + what + ".png") for t in titles]
        else:
            titles = [title]
            path = [path.joinpath(what + ".png")]

        bbox = [
            da.x.min().values,
            da.x.max().values,
            da.y.min().values,
            da.y.max().values,
        ]
        aspect_ratio = (bbox[1] - bbox[0]) / (bbox[3] - bbox[2])

        vmin = self._custom_round(da.min(), 5, mode="floor").values
        vmax = self._custom_round(da.max(), 5, mode="ceil").values

        if minmax:
            vmin = minmax[0]
            vmax = minmax[1]

        for i in range(len(titles)):
            fig = plt.figure(figsize=(25 * aspect_ratio, 20))
            ax = fig.add_subplot(111)

            if type(bbox) == list:
                ax.set_xlim([bbox[0], bbox[1]])
                ax.set_ylim([bbox[2], bbox[3]])
            else:
                print("bbox not given or not understood, using defaults")

            # Add shapefiles and labels if applicable
            if type(shapefiles) == list:
                for j, shape in tqdm(enumerate(shapes_to_plot), "Plotting figures..."):
                    shape = shape.to_crs(self.crs)
                    if shape.type[0] == "Polygon":
                        shape.boundary.plot(
                            ax=ax,
                            color=shapefile_colors[j],
                            linewidth=shapefile_widths[j],
                            label=shapefile_names[j],
                        )
                    else:
                        shape.plot(
                            ax=ax,
                            color=shapefile_colors[j],
                            linewidth=shapefile_widths[j],
                            label=shapefile_names[j],
                        )

                    if shapefile_labels != None:
                        bounds = shape.geometry.bounds
                        texts = []
                        if shapefile_labels[j] != None:
                            for t, label in zip(
                                bounds.iterrows(), shape[shapefile_labels[j]]
                            ):
                                x = t[1].maxx
                                y = t[1].maxy
                                if (
                                    x > bbox[0]
                                    and x < bbox[1]
                                    and y > bbox[2]
                                    and y < bbox[3]
                                ):
                                    texts.append(
                                        plt.text(x - 8, y + 15, label, fontsize=18)
                                    )

            # TODO self.crs Add basemap if specified
            if basemap:
                ctx.add_basemap(
                    ax, crs=self.crs, source=ctx.providers.OpenStreetMap.Mapnik
                )

            # Add images and colorbars
            if single_or_multi == "multiple":
                plotfunc(da.isel(year=i), fig, ax, cmap, vmin=vmin, vmax=vmax)
            else:
                plotfunc(da, fig, ax, cmap, vmin=vmin, vmax=vmax)

            # Arange ticks and labels
            if tick_interval != None:
                # try:
                float(tick_interval)
                startx = self._custom_round(ax.get_xlim()[0], tick_interval)
                starty = self._custom_round(ax.get_ylim()[0], tick_interval)
                ax.set_xticks(
                    np.arange(startx, bbox[1] + 1, tick_interval, dtype=np.int32)
                )
                ax.set_yticks(
                    np.arange(starty, bbox[3] + 1, tick_interval, dtype=np.int32)
                )
                # except ValueError:
                #     print('tick_interval not understood, using defaults')
            ax.set_xticklabels(ax.get_xticks(), fontsize=18, rotation=0, ha="center")
            ax.set_yticklabels(ax.get_yticks(), fontsize=18, rotation=90, va="center")
            ax.set_xlabel("x [m]", fontsize=20)
            ax.set_ylabel("y [m]", fontsize=20)

            if grid:
                plt.grid(alpha=0.3, color="k")

            plt.gca().set_aspect("equal", adjustable="box")

            try:
                ax.set_title(str(titles[i]), fontsize=24)
            except IndexError:
                pass

            if anim and single_or_multi == "multiple":
                canvas = FigureCanvas(fig)
                canvas.draw()
                width, height = np.int32(fig.get_size_inches() * fig.get_dpi())
                list_of_images.append(
                    np.frombuffer(canvas.tostring_rgb(), dtype="uint8").reshape(
                        height, width, 3
                    )
                )
            else:
                plt.savefig(path[i], bbox_inches="tight")

            plt.close()

        if anim:
            # animate.image_list_to_movie(list_of_images, path[0], anim_fps)
            pass

    def plot_3d(self, what, path, titles=None):

        options = self.data_map[what]
        da = options[0]
        title = options[1]
        single_or_multi = options[2]
        cmap = options[3]
        path = Path(path)
        offset = -200

        titles = da[self.tname].values.astype("datetime64[Y]").astype(int) + 1970
        titles = [
            title + " " + t.split("-")[0] + "-" + t.split("-")[1]
            for t in np.datetime_as_string(da[self.tname].values)
        ]
        path = [path.joinpath(str(t) + "_" + what + ".png") for t in titles]

        for i in range(len(titles[offset:])):
            fig = plt.figure(figsize=(14, 14))
            ax = plt.axes(projection="3d")

            y = np.outer(da.y.values, np.ones(len(da.x)))
            x = np.outer(da.x.values, np.ones(len(da.y))).transpose()

            # Creating plot
            surf = ax.plot_surface(
                x,
                y,
                da[offset + i].values,
                cmap=cmap,
                edgecolor="none",
                rcount=100,
                ccount=100,
                shade=True,
                vmin=-20,
                vmax=0,
            )
            ax.set_aspect("auto")
            ax.set_zlim(-20, 0)
            ax.azim = -0
            ax.elev = 20
            ax.set_xlabel("x [m]")
            ax.set_ylabel("y [m]")
            ax.set_zlabel("\nz [m]")

            plt.xticks(fontsize=8)
            plt.yticks(fontsize=8)

            ax.set_title(str(titles[offset + i]), fontsize=20)
            plt.savefig(path[offset + i], bbox_inches="tight")

    def plot_bathymetry_single(
        self, gdf_row, dx, path, xlim=[], ylim=[], interp=False, add_min_bathy=False
    ):

        if interp:
            what = "z_interp"
        else:
            what = "z"

        self.shape_to_samplepoints(gdf_row, dx)
        self.minimum()
        total_min = self.z_min.sel(
            y=self.y_samples[0], x=self.x_samples[0], method="nearest"
        ).values

        options = self.data_map[what]
        da = options[0]
        length = len(da[self.tname])
        title = options[1]
        single_or_multi = options[2]

        titles = da[self.tname].values.astype("datetime64[Y]").astype(int) + 1970
        titles = [
            title + " " + t.split("-")[0] + "-" + t.split("-")[1]
            for t in np.datetime_as_string(da[self.tname].values)
        ]
        path = [path.joinpath(str(t) + "_" + " profile.png") for t in titles]

        xdata = np.arange(0, dx * len(self.x_samples[0]), dx)
        for i in range(length):
            fig = plt.figure(figsize=(18, 14))
            ax = fig.add_subplot(1, 1, 1)
            if add_min_bathy:
                da_min = self.mincust(da[: i + 1, :, :])
                if da_min.size != 0:
                    ydata_min = da_min.sel(
                        y=self.y_samples[0], x=self.x_samples[0], method="nearest"
                    ).values
                    plt.plot(
                        xdata,
                        ydata_min,
                        color="red",
                        label=self.data_map["min"][1],
                        linestyle=(0, (2, 1)),
                        linewidth=1.5,
                    )

            ydata = (
                da.isel(year=i)
                .sel(y=self.y_samples[0], x=self.x_samples[0], method="nearest")
                .values
            )
            plt.plot(xdata, ydata, color="darkorange", label=title)

            if len(xlim) == 2:
                ax.set_xlim(xlim[0], xlim[1])
            else:
                ax.set_xlim(np.min(xdata), np.max(xdata))
            if len(ylim) == 2:
                ax.set_ylim(ylim[0], ylim[1])
            else:
                ax.set_ylim(self._custom_round(np.min(total_min), 2, mode="floor"), 2)

            # ax.set_ylim(-24, 2) ### BORNDIEP ONLY
            plt.fill_between(
                xdata,
                0,
                ydata,
                where=(ydata <= 0),
                color="deepskyblue",
                alpha=0.3,
                interpolate=True,
            )
            plt.fill_between(xdata, ax.get_ylim()[0], ydata, color="gold", alpha=0.3)

            # Borndiep special
            ##################################
            # top_potclay = np.full(len(xdata), -21, dtype=np.float32)
            # bottom_potclay = np.full(len(xdata), -23, dtype=np.float32)
            # top_hclay = np.full(len(xdata), -10.5, dtype=np.float32)
            # bottom_hclay = np.full(len(xdata), -11.5, dtype=np.float32)

            # top_hclay[top_hclay>ydata_min] = ydata_min[top_hclay>ydata_min]
            # top_potclay[top_potclay>ydata_min] = ydata_min[top_potclay>ydata_min]

            # plt.fill_between(xdata, top_potclay, bottom_potclay, where=(ydata_min>=bottom_potclay), color='green', alpha=0.5, interpolate=True)
            # plt.fill_between(xdata, top_hclay, bottom_hclay, where=(ydata_min>=bottom_hclay), color='green', alpha=0.5, interpolate=True)

            # plt.text(100, -22.2, '\'Potklei\'', fontsize=14)
            # plt.text(100, -11.15, 'Vroeg-Holocene klei', fontsize=14)
            # plt.text(1750, 1, 'Kust van Ameland -->', fontsize=14)
            ##################################

            ax.legend(fontsize=18, loc=2)  ## loc=2 only for BORNDIEP!
            ax.set_xticklabels(ax.get_xticks(), fontsize=18, rotation=0, ha="center")
            ax.set_yticklabels(ax.get_yticks(), fontsize=18, rotation=90, va="center")
            ax.set_xlabel("Afstand langs profiel (m)", fontsize=16)
            ax.set_ylabel("Hoogte (m NAP)", fontsize=16)
            ax.set_title(titles[i], fontsize=20)
            ax.grid(alpha=0.5)
            plt.savefig(path[i], bbox_inches="tight")
            plt.close()

    def plot_profile_multi(
        self,
        what,
        shape,
        title,
        dx=20,
        xlim=[],
        ylim=[],
        geological_layers=[],
        geological_layers_thickness=[],
        geological_layers_colors=[],
        geological_layers_names=[],
        deposition_age=False,
    ):

        options = self.data_map[what]
        da = options[0]
        title = options[1]
        single_or_multi = options[2]

        self.shape_to_samplepoints(shape, dx)
        self.minimum()

        if ylim == []:
            ymin = da.min.values
            ymax = da.max.values
        else:
            ymin = ylim[0]
            ymax = ylim[1]

        layers = []
        layers_t = []
        for j, layer in enumerate(geological_layers):
            layers.append(self.imod_read_transform_geological_layers(layer, 32631))
            layers_t.append(
                self.imod_read_transform_geological_layers(
                    geological_layers_thickness[j], 32631
                )
            )

        colors = plt.cm.viridis(np.linspace(0, 1, len(da)))

        for p in range(len(self.x_samples)):
            fig = plt.figure(figsize=(36, 14))
            ax = fig.add_subplot(1, 1, 1)
            xdata = np.arange(0, dx * len(self.x_samples[p]), dx)
            ydatas = []
            drawn_lines = 0
            for i, line in enumerate(da):
                ydata = line.sel(
                    y=self.y_samples[p], x=self.x_samples[p], method="nearest"
                ).values
                ydatas.append(ydata)
                if any(np.isfinite(ydata)):
                    plt.plot(
                        xdata,
                        ydata,
                        color=colors[i],
                        label=str(line[self.tname].values).split("T")[0],
                        alpha=0,
                    )
                    drawn_lines += 1
                else:
                    continue

                if deposition_age:
                    if drawn_lines == 1:
                        plt.fill_between(
                            xdata,
                            ydata,
                            ymin,
                            color=colors[i],
                            alpha=1,
                            interpolate=True,
                        )
                    else:
                        # Fill color for deposited material
                        plt.fill_between(
                            xdata,
                            ydata,
                            ydatas[i - 1],
                            where=(ydata >= ydatas[i - 1]),
                            color=colors[i],
                            alpha=1,
                            hatch=".",
                            interpolate=True,
                        )
                        # Remove color if deposit has eroded
                        plt.fill_between(
                            xdata,
                            ymax,
                            ydata,
                            color="white",
                            alpha=1,
                            interpolate=True,
                        )

            ydata_min = self.z_min.sel(
                y=self.y_samples[p], x=self.x_samples[p], method="nearest"
            ).values
            # plt.plot(
            #     xdata, ydata_min, color="k", label="Minimum", linestyle=(0, (2, 1))
            # )

            for l, l_t, c, name in zip(
                layers, layers_t, geological_layers_colors, geological_layers_names
            ):
                ydata_l = l.sel(
                    y=self.y_samples[p], x=self.x_samples[p], method="nearest"
                ).values
                ydata_t = l_t.sel(
                    y=self.y_samples[p], x=self.x_samples[p], method="nearest"
                ).values

                top = ydata_l
                bottom = ydata_l - ydata_t

                top[top > ydata_min] = ydata_min[top > ydata_min]

                plt.fill_between(
                    xdata,
                    top,
                    bottom,
                    where=(ydata_min >= bottom),
                    color=c,
                    alpha=0.5,
                    interpolate=True,
                )

                plt.plot(xdata, ydata_l, color=c, linewidth=2, label=name)

            if len(xlim) == 2:
                ax.set_xlim([xlim[0], xlim[1]])
            else:
                ax.set_xlim([xdata[0], xdata[-1]])
            if len(ylim) == 2:
                ax.set_ylim([ymin, ymax])
            else:
                pass

            # Ensure only unique labels
            handles, labels = plt.gca().get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            ax.legend(
                by_label.values(),
                by_label.keys(),
                bbox_to_anchor=(1.05, 1),
                loc="upper left",
                borderaxespad=0.0,
                fontsize=16,
            )

            ax.set_xticklabels(ax.get_xticks(), fontsize=18, rotation=0, ha="center")
            ax.set_yticklabels(ax.get_yticks(), fontsize=18, rotation=90, va="center")
            ax.set_xlabel("Afstand langs profiel (m)", fontsize=16)
            ax.set_ylabel("Hoogte (m NAP)", fontsize=16)
            ax.set_title(title + ": Profiel " + str(self.id_samples[p]), fontsize=20)
            ax.text(
                0, 1.01, self.llabel[p], weight="bold", size=18, transform=ax.transAxes
            )
            if len(self.rlabel) == 1:
                ax.text(
                    0.99,
                    1.01,
                    self.rlabel[p],
                    weight="bold",
                    size=18,
                    transform=ax.transAxes,
                )
            else:
                ax.text(
                    0.98,
                    1.01,
                    self.rlabel[p],
                    weight="bold",
                    size=18,
                    transform=ax.transAxes,
                )
            ax.grid(alpha=0.5)
            plt.savefig(
                title + "_profiel_" + str(self.id_samples[p]), bbox_inches="tight"
            )
            plt.close()

    def data_to_nc(self, what, file):
        if self.data_map[what][0] is not None:
            self.data_map[what][0].to_netcdf(file)

    def data_to_tiff(self, what, file):
        if self.data_map[what][0] is not None and len(self.data_map[what][0].dims) == 2:
            imod.rasterio.write(file, self.data_map[what][0])

    @staticmethod
    def _custom_round(target, interval, mode="ceil"):
        if target % interval != 0.0:
            if mode == "floor":
                return target + (interval - (target % interval)) - interval
            else:
                return target + (interval - (target % interval))
        else:
            return target

    def _plot_bathymetry(self, da, fig, ax, cmap, vmin="auto", vmax="auto"):
        img = da.plot.imshow(
            add_colorbar=True,
            cmap=cmap,
            vmax=vmax,
            vmin=vmin,
            cbar_kwargs={"extend": "neither"},
        )

        cbar = img.colorbar
        cbar.ax.tick_params(labelsize=18)

    def _plot_difference(self, da, fig, ax, cmap, vmin="auto", vmax="auto"):
        img = da.plot.imshow(
            add_colorbar=True,
            cmap=cmap,
            vmax=vmax,
            vmin=vmin,
            cbar_kwargs={"extend": "neither"},
        )

        cbar = img.colorbar
        cbar.ax.tick_params(labelsize=18)

    def wind_label(self, xp, yp, xl, yl):
        angle = atan2(xp - xl, yp - yl)
        bearing = (degrees(angle) + 360) % 360

        wind_dir = windlabels[len(winddirs) - np.argmax(bearing > winddirs[::-1]) - 1]

        return (bearing, wind_dir)

    def imod_read_transform_geological_layers(self, path, src):
        ascgrid = imod.rasterio.read(path)
        ascgrid = ascgrid.where(ascgrid != ascgrid.attrs["nodatavals"][0], other=np.nan)
        return imod.prepare.reproject(
            ascgrid, src_crs={"init": f"EPSG:{src}"}, dst_crs=self.crs
        )


nc_file = r"n:\Projects\11204500\11204986\B. Measurements and calculations\Bathymetry\waal_test4_timeseries.nc"
mosaic = r"n:\Projects\11204500\11204986\B. Measurements and calculations\Bathymetry\waal_test_area1_mosaic.tiff"

m = raster_morphology(nc_file, mosaic)
m.plot_profile_multi(
    "z",
    r"n:\Projects\11204500\11204986\B. Measurements and calculations\Bathymetry\Profielen\vc10.shp",
    "waal",
    dx=0.5,
    ylim=[-4, 0],
    deposition_age=True,
)
m.interpolate(refine_extra=8)

m.interp_da.to_netcdf(
    r"p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Kaart\experimenteel\Wielingen\Wielingen_interp4.nc"
)
m.dzdt


# folder_p = Path(r'p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\afgeleide rasterdata\Geulbodem')
folder_p = Path(
    r"p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\afgeleide rasterdata\Gebiedsbreed"
)
fold = Path(
    r"p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Callantsoog\data"
)
fold = Path(
    r"p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Terschelling\data_vakl"
)
fold = Path(
    r"p:\1204421-kpp-benokust\GIS_data\bathymetrie\Vaklodingen\Mosaicen\_Totaal_Mosaic"
)
fold = Path(r"p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Kaart\Data")
# fold = Path(r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Bathy\WS_monding')
# profiles = gpd.read_file(Path(r'p:\11206685-nam-lidar-2020-2021\02_data\05_profielen\profielen_2020_2021.shp'))

dataps = [nc for nc in fold.glob("*.nc")]
current = None
current = ["wadden_oost", "wadden_west", "w", "hk_noord", "hk_zuid"]
shapename = [
    "ERL_profiles",
    "ERL_profiles_ww",
    "ERL_profiles_wz",
    "ERL_profiles_hkn",
    "ERL_profiles_hkz",
]


for c, sn in zip(current, shapename):
    for datap in dataps:
        if c != None:
            if datap.stem != c:
                continue

        mosaic = datap.parent.joinpath(datap.stem + "_mosaic.tiff")

        m = raster_morphology(datap, mosaic)
        # m.minimum()
        # m.interpolate(refine_extra=4)
        # m.greatest_diff()
        # m.diff()
        # m.current_diff_from_deepest()
        # m.slope()
        # m.potential_NEL_area()

        # fold_out = Path(r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Kaart\Data\texelstroom')
        # m.plot_3d('z_interp',
        #             fold_out,
        #             )
        # outfile = Path(fold_out).joinpath(str(fold_out.stem) + '_bathymap_animation.mp4')
        # wildcard = '*z_interp.png'
        # animate.image_files_to_movie(fold_out, outfile, 10, wildcard=wildcard)

        shape = (
            r"p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Kaart\Productie kaart\\"
            + sn
            + ".shp"
        )
        m.plot_profile_multi("z", shape, "test", dx=20)

    # for what, minmax in zip(['min', 'max_diff', 'dzdt_max', 'dzdt_mean', 'min_current', 'nel_z', 'slope1', 'slope2'],
    #                         [None, [0, 10], None, None, [0, 10], None, [0, 5], [0, 5]]):

    #     towrite = m.data_map[what][0][::-1, :]
    #     towrite.to_netcdf(r'p:\1204421-kpp-benokust\GIS_data\bathymetrie\Vaklodingen\Mosaicen\_Totaal_Mosaic\nederland_alle_vaklodingen_tot_2020_minimum.nc')
    #     towrite['y'] = m.z_min.y[::-1]

    #     m.data_to_tiff(what, fold.joinpath(datap.stem + '_' + what + '.tiff'))

    # m.plot_2d(what,
    #     fold,
    #     basemap = True,
    #     grid = True,
    #     shapefiles = [# r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Terschelling\xsb\shape\onderzoeksgebied.shp',
    #                     # r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Terschelling\Profielen.shp',
    #                 #r'p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\Locations\Geologische Invloed Geulwand.shp',
    #                 #r'p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\afgeleide rasterdata\Geulbodem\profiles.shp'
    #                 ],
    #     shapefile_names = [#'Onderzoeksgebied',
    #                         #'Invloed Geulbodem',
    #                         #'Invloed Geulwand',
    #                         #'Profielen morfologie'
    #                         ],
    #     shapefile_widths = [2, 2, 2, 1],
    #     shapefile_colors = ['k', 'r', 'r', 'k'],
    #     shapefile_styles = ['-', ':', '-', '-'],
    #     minmax = minmax,
    #     tick_interval = 20000)

    # shape = r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Bathy\profielen_jebbe.shp'
    # m.plot_profile_multi( 'z', shape, 'test', dx=50,
    #                       geological_layers=[r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Top_reflector1.tiff',
    #                                         r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Top_reflector2.tiff',
    #                                         r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Top_hard1.tiff',
    #                                         r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Top_schelpenlaag.tiff'],
    #                       geological_layers_thickness = [r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Dikte_reflector1.tiff',
    #                                                     r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Dikte_reflector2.tiff',
    #                                                     r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Dikte_hard1.tiff',
    #                                                     r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\Seismiek\Analysis\v12_4km\Dikte_schelpenlaag.tiff'],
    #                       geological_layers_colors = ['green', 'red', 'purple', 'blue'],
    #                       geological_layers_names = ['Reflector1', 'Reflector2', 'Hard1', 'Schelpenlaag'])

    # shape = r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Terschelling\Profielen.shp'

    # shape = profiles[profiles['Naam']==str(fold.stem)]
    # m.interpolate(refine_extra=4)
    # shape = r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Callantsoog\data\Profiel_1708.shp'
    # m.plot_bathymetry_single(shape, 20, fold, xlim=[0, 2000], ylim=[-12.5, 0], interp=True, add_min_bathy=True)

    # outfile = Path(fold).joinpath(str(fold.stem) + '_profile_animation.mp4')
    # wildcard = '*profile.png'
    # animate.image_files_to_movie(fold, outfile, 10, wildcard=wildcard)

    # m.plot_2d('z_interp',
    #     fold,
    #     basemap = True,
    #     grid = True,
    #     shapefiles = [#r'n:\Projects\11203500\11203725\B. Measurements and calculations\01_sedimentpilot_monding\08_ondergrond\WS Mondingsgebied.shp',
    #                         #r'p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\Locations\Geologische Invloed Geulbodem.shp',
    #                       #r'p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\Locations\Geologische Invloed Geulwand.shp',
    #                       #r'p:\1204421-kpp-benokust\2021\3-MorfologieKust\3A1 - Geologie\Case studies\Callantsoog\data\Profielen.shp'
    #                       ],
    #     shapefile_names = [#'Onderzoeksgebied',
    #                         #'Invloed Geulbodem',
    #                         #'Invloed Geulwand',
    #                         #'Profiellocaties'
    #                         ],
    #     shapefile_widths = [2, 1.5],
    #     shapefile_colors = ['r', 'k'],
    #     shapefile_styles = ['-', '-'],
    #     shapefile_labels = [None, 'id'],
    #     minmax = [-30, 0],
    #     tick_interval = 2000)

    # outfile = Path(fold).joinpath(str(fold.stem) + '_bathymap_animation.mp4')
    # wildcard = '*z_interp.png'
    # animate.image_files_to_movie(fold, outfile, 10, wildcard=wildcard)


# imod.rasterio.write(r'p:\1204421-kpp-benokust\2020\3-MorfologieKust\3A1 - Geologie\afgeleide rasterdata\wadden_gdiff.tiff', m.gdiff, driver='GTiff')
