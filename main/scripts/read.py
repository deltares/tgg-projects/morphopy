from pathlib import Path
import numpy as np
import pandas as pd
import xarray as xr
import xugrid
from scipy.stats import mode
import imod
from scripts import utils


def read_raster(raster_file, reader="xarray"):
    if reader == "xarray":
        da = xr.open_rasterio(raster_file)
        da = da.bfill("band").squeeze()
    elif reader == "imod":
        da = imod.rasterio.open(raster_file)
    return da


def read_xyz(xyz_file, method="structured"):
    if Path(xyz_file).suffix in [".xyz", ".txt", ".csv", ".pts"]:
        file = Path(xyz_file)
    else:
        raise NameError

    # Reading the XYZ-file
    sep = utils.find_xyz_sep(xyz_file)
    if sep == " ":
        df = pd.read_csv(
            file, header=None, names=["x", "y", "z"], delim_whitespace=True
        )
    else:
        df = pd.read_csv(file, sep=sep, header=None, names=["x", "y", "z"])

    data = utils.remove_df_header(df)
    diff = np.diff(data.x)
    res = mode(diff[diff != 0])[0][0]

    if method == "structured":
        da = __xyz_structured(data, res)
        return da
    elif method == "unstructured":
        grid = __xyz_unstructured(data.x, data.y, res, res)
        da = xr.DataArray(
            data=data.z,
            dims=[grid.face_dimension],
        )
        uda = xugrid.UgridDataArray(da, grid)
        return uda


def __xyz_structured(data, res):
    bounds = (
        np.nanmin(data.x),
        np.nanmax(data.x),
        np.nanmin(data.y),
        np.nanmax(data.y),
    )

    sorted_data = data.sort_values(["y", "x"], ascending=[False, True])

    idxs_x = np.int32(np.round((sorted_data["x"] - bounds[0]) / res))
    idxs_y = np.int32(np.round((sorted_data["y"] - bounds[2]) / res))

    coord_x = np.arange(bounds[0], bounds[1] + res, res)
    coord_y = np.arange(bounds[3], bounds[2] - res, -res)

    # Allocate array
    array = np.zeros((len(coord_y), len(coord_x)))

    # Fill array
    array[idxs_y, idxs_x] = sorted_data["z"].values
    array[array == 0] = np.nan
    array = array[::-1, :]
    da = xr.DataArray(data=array, coords={"y": coord_y, "x": coord_x}, dims=["y", "x"])
    da = utils.set_da_attributes(da)
    return da


def __xyz_unstructured(x: np.ndarray, y: np.ndarray, dx: float, dy: float):
    if x.size != y.size:
        raise ValueError(
            f"size of x and y must match, received x {x.size} versus y {y.size}"
        )
    nface = x.size
    face_node_connectivity = np.arange(nface * 4).reshape((nface, 4))
    # Compute coordinates of corners
    left = x - 0.5 * dx
    right = x + 0.5 * dx
    lower = y - 0.5 * dy
    upper = y + 0.5 * dy
    # Create a (memory contiguous) array of vertices
    node_coordinates = np.empty((nface, 4, 2), dtype=np.float64)
    # Set the nodes in a counter-clockwise manner
    node_coordinates[:, 0, 0] = left
    node_coordinates[:, 0, 1] = lower
    node_coordinates[:, 1, 0] = right
    node_coordinates[:, 1, 1] = lower
    node_coordinates[:, 2, 0] = right
    node_coordinates[:, 2, 1] = upper
    node_coordinates[:, 3, 0] = left
    node_coordinates[:, 3, 1] = upper
    node_coordinates = node_coordinates.reshape((nface * 4, 2))
    # Now identify unique pairs, and get the inverse.
    node_coordinates, inverse = np.unique(node_coordinates, return_inverse=True, axis=0)
    face_node_connectivity = inverse[face_node_connectivity]
    return xugrid.Ugrid2d(
        node_x=node_coordinates[:, 0],
        node_y=node_coordinates[:, 1],
        fill_value=-1,
        face_node_connectivity=face_node_connectivity,
    )


# x = np.array([0.5, 0.5, 1.5])
# y = np.array([2.5, 3.5, 2.5])
# grid = read_xyz_unstructured(x, y, 1.0, 1.0)
# da = xr.DataArray(
#     data=[0.0, 1.0, 2.0],
#     dims=[grid.face_dimension],
# )
# uda = xugrid.UgridDataArray(da, grid)
# uda.ugrid.plot()

# da = read_xyz(
#     r"n:\Projects\11204500\11204986\B. Measurements and calculations\Bathymetry\20210601_PES WAAL__S1_Site 2_25cm_RD_NAP.txt",
#     method="unstructured",
# )

# da = read_xyz(
#     r"n:\Projects\11204500\11204986\B. Measurements and calculations\Bathymetry\20210601_PES WAAL__S1_Site 2_25cm_RD_NAP.txt",
#     method="structured",
# )
# print("stop")

# ds = da.ugrid.to_dataset()
# ds = imod.util.mdal_compliant_ugrid2d(ds)
# ds.to_netcdf(
#     r"n:\Projects\11204500\11204986\B. Measurements and calculations\Bathymetry\test.nc"
# )
