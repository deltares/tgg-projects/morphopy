import xarray as xr
import imod
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
from raster_math import (
    trend_per_cell,
    trend_per_cellneighbours,
    mean_over_areas,
    data_over_area,
)
import time


class Analysis:
    def __init__(
        self,
        path_to_nc,
        path_to_mosaic,
        timeaxis=0,
        chunk=True,
        chunksize="auto",
        crs=28992,
    ):
        self.ta = timeaxis
        self.crs = f"epsg:{str(crs)}"

        if chunk:
            self.data = xr.open_dataarray(
                path_to_nc, chunks={"year": 5, "x": "auto", "y": "auto"}
            )
            self.chunks = tuple([c[0] for c in self.data.chunks])
            self.mosaic = (
                xr.open_rasterio(path_to_mosaic, chunks=self.chunks)
                .squeeze()
                .drop_vars("band")
            )
        else:
            self.data = xr.open_dataarray(path_to_nc)
            self.chunks = None
            self.mosaic = xr.open_rasterio(path_to_mosaic)

    @property
    def nx(self):
        return np.int64(self.data.sizes["x"])

    @property
    def ny(self):
        return np.int64(self.data.sizes["y"])

    @property
    def res(self):
        try:
            return np.float32(self.data.res[0])
        except AttributeError:
            return np.float32(st.mode(np.diff(self.data["x"]))[0][0])

    @property
    def tname(self):
        return self.data.dims[self.ta]

    def trend_cells(self, mode="single", areas=None):
        data = self.data.values
        timeaxis = np.cumsum(
            np.diff(self.data[self.tname].values).astype("timedelta64[D]")
        ).astype(np.int64)
        timeaxis = np.insert(timeaxis, 0, 0)
        trend_array = np.zeros_like(data[0, :, :])
        trend_array[:, :] = np.nan
        surveys_array = np.full_like(trend_array, np.nan)
        rsq_array = np.full_like(trend_array, np.nan)

        t0 = time.perf_counter()
        if mode == "single":
            trend_array, surveys_array, rsq_array = trend_per_cell(
                data, timeaxis, self.ny, self.nx, trend_array, surveys_array, rsq_array
            )
        elif mode == "cluster":
            trend_array, surveys_array, rsq_array = trend_per_cellneighbours(
                data, timeaxis, self.ny, self.nx, trend_array, surveys_array, rsq_array
            )
        dt = time.perf_counter() - t0

        self.trend_da = self.ndarray_to_dataarray(trend_array)
        self.surveys_da = self.ndarray_to_dataarray(surveys_array)
        self.rsq_da = self.ndarray_to_dataarray(rsq_array)

        if areas is not None:
            self.trend_areas = mean_over_areas(areas, self.trend_da, "vak_id")
            self.rsq_areas = mean_over_areas(areas, self.rsq_da, "vak_id")

        # self.trend_da.plot.imshow()
        # self.surveys_da.plot.imshow()
        # self.rsq_da.plot.imshow()

    def ndarray_to_dataarray(self, ndarray):
        da = xr.DataArray(
            ndarray, coords={"y": self.data.y, "x": self.data.x}, dims=("y", "x")
        )
        da.attrs["nodatavals"] = np.nan
        da["x"].attrs["axis"] = "X"
        da["y"].attrs["axis"] = "Y"
        return da


analysis = Analysis(
    r"p:\kpp-benokust-gis-data\bathymetrie\temp\zandbalans_nh_timeseries.nc",
    r"p:\kpp-benokust-gis-data\bathymetrie\temp\zandbalans_nh_mosaic.tiff",
)

analysis.trend_cells(
    mode="cluster",
    areas=r"p:\kpp-benokust-gis-data\bathymetrie\temp\Zandbalans\Vakken\vakken_vakl.shp",
)


analysis.trend_areas.to_file(
    r"p:\kpp-benokust-gis-data\bathymetrie\temp\Zandbalans\Shapefiles_balans\zandbalans_vakken_nh_vakl.shp"
)
analysis.rsq_areas.to_file(
    r"p:\kpp-benokust-gis-data\bathymetrie\temp\Zandbalans\Shapefiles_balans\rsq_vakken_nh_vakl.shp"
)
# imod.rasterio.write(
#     r"p:\kpp-benokust-gis-data\bathymetrie\temp\Zandbalans\zandbalans_nh_navy_surveys.tiff",
#     self.surveys_da,
# )
