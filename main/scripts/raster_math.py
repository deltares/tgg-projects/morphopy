import numpy as np
import numba
from typing import Tuple
import imod
import geopandas as gpd


@numba.njit
def __linear_interpolator(d, t) -> Tuple[np.float64, np.float64, np.float64]:
    n = len(d)
    a = (n * np.sum(t * d) - np.sum(t) * np.sum(d)) / (
        n * np.sum(t * t) - np.sum(t) ** 2
    )
    b = (np.sum(d) - a * np.sum(t)) / n
    ss_res = np.sum((d - (a * t + b)) ** 2)
    ss_tot = np.sum((d - np.mean(d)) ** 2)
    if ss_tot != 0.0:
        rsq = 1 - (ss_res / ss_tot)
    else:
        rsq = np.nan

    return a, b, rsq


@numba.njit
def trend_per_cell(
    y: np.ndarray,
    x: np.ndarray,
    ny: np.int64,
    nx: np.int64,
    trend_array: np.ndarray,
    observations_array: np.ndarray,
    rsq_array: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    for yi in range(ny - 1):
        for xi in range(nx - 1):
            d = y[:, yi, xi]
            t = x[np.isfinite(d)]
            if len(t) > 2:
                d = d[np.isfinite(d)]
                a, b, rsq = __linear_interpolator(d, t)
                trend_array[yi, xi] = a
                observations_array[yi, xi] = len(d)
                rsq_array[yi, xi] = rsq

    return trend_array, observations_array, rsq_array


# @numba.njit
def trend_per_cellneighbours(
    y: np.ndarray,
    x: np.ndarray,
    ny: np.int64,
    nx: np.int64,
    trend_array: np.ndarray,
    observations_array: np.ndarray,
    rsq_array: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    for yi in range(ny - 1):
        for xi in range(nx - 1):
            if yi > 0 and xi > 0 and yi < ny - 1 and xi < nx - 1:
                d1 = y[:, yi, xi]
                d2 = y[:, yi, xi + 1]
                d3 = y[:, yi - 1, xi + 1]
                d4 = y[:, yi - 1, xi]
                d5 = y[:, yi - 1, xi - 1]
                d6 = y[:, yi, xi - 1]
                d7 = y[:, yi + 1, xi - 1]
                d8 = y[:, yi + 1, xi]
                d9 = y[:, yi + 1, xi + 1]

                t1 = x[np.isfinite(d1)]
                t2 = x[np.isfinite(d2)]
                t3 = x[np.isfinite(d3)]
                t4 = x[np.isfinite(d4)]
                t5 = x[np.isfinite(d5)]
                t6 = x[np.isfinite(d6)]
                t7 = x[np.isfinite(d7)]
                t8 = x[np.isfinite(d8)]
                t9 = x[np.isfinite(d9)]

                d = np.hstack((d1, d2, d3, d4, d5, d6, d7, d8, d9))
                t = np.hstack((t1, t2, t3, t4, t5, t6, t7, t8, t9))

                if len(np.unique(t)) > 1:
                    d = d[np.isfinite(d)]
                    a, b, rsq = __linear_interpolator(d, t)
                    trend_array[yi, xi] = a
                    observations_array[yi, xi] = len(t1)
                    rsq_array[yi, xi] = rsq

    return trend_array, observations_array, rsq_array


def mean_over_areas(areas, raster, column_name):
    gdf = gpd.read_file(areas)
    mean = np.zeros(len(gdf))
    areas_array = imod.prepare.rasterize(gdf, like=raster, column=column_name)

    for i, area in enumerate(gdf[column_name]):
        raster_area = raster.where(areas_array == area)
        mean[i] = raster_area.mean().values

    gdf["mean"] = mean
    return gdf


def data_over_area(areas, raster, column_name, yld="data"):
    gdf = gpd.read_file(areas)
    mean = np.zeros(len(gdf))
    areas_array = imod.prepare.rasterize(gdf, like=raster[-1, :, :], column=column_name)

    for i, area in enumerate(gdf[column_name]):
        raster_area = raster.where(areas_array == area)
        indices_with_data = [
            i
            for i in range(len(raster_area.year))
            if np.isfinite(raster_area[i, :, :]).any()
        ]
        raster_area = raster_area[indices_with_data, :, :]

        if yld == "data":
            yield raster_area


def clip_nan(da):
    np.isnan(da).all(axis=1)


# import matplotlib.pyplot as plt

# plt.scatter(t, d)
# vec = np.arange(0, t[-1], 10)
# plt.plot(vec, a * vec + b)
