import numpy as np
import pandas as pd
from datetime import datetime

seps = [
    "\t",
    "    ",
    "   ",
    "  ",
    " ",
    "|",
    ",",
    ";",
    "-",
    "_",
]  # All seperators I can currently think of


def find_xyz_sep(xyz_file, seps=seps):
    with open(xyz_file) as file:
        for i in range(2):
            line = file.readline()
        sep = []
        [sep.append(char) for char in line if char in seps]
        sep = sep[1]
    return sep


def remove_df_header(df):
    for i in range(len(df)):
        entry = df.iloc[[i], [i]].values[0][0]
        try:
            entry = float(entry)
        except ValueError:
            continue
        if type(entry) == float:
            df = df[i:]
            df = df.astype(np.float32)
            return df


def find_date_in_filename_from_format(filename, time_format):
    filename_parts = filename.stem.split("_")
    time = None
    for filename_part in filename_parts:
        try:
            time = datetime.strptime(filename_part, time_format)
        except ValueError:
            continue
    if time is not None:
        return time
    else:
        raise ValueError(
            f"No date in given format was found in file: '{filename.stem}'"
        )

        # for i in range(len(stamps)):
        # if stamps[i][-2:] == "00" and self.tformat == "%y%m%d":
        #     stamps[i] = stamps[i][:-2] + "01"


def set_da_attributes(da):
    da.attrs["nodatavals"] = np.nan
    da["x"].attrs["axis"] = "X"
    da["y"].attrs["axis"] = "Y"
    return da
