# -*- coding: utf-8 -*-
"""
Raster preprocessing using for Xarray + Dask for processing larger-than-memory datasets
and some Imod-Python rasterio and regrid functions to transform data.

Developed for the Deltares MorphoPy package. 

Methods in this file include:
    - Loading and converting rasters to the same grid and coordinate reference system
    - Determining dt between rasters
    - Creating a single DataArray from multi-temporal raster data + export to NetCDF
    - Creating mosaic data, providing both the mosaiced data and a raster indicating which 'year' lies on top
    - A method to combine and export tiled data (non-overlapping). e.g. multiple AHN tiles 
"""
import imod
from pathlib import Path
import xarray as xr
from datetime import datetime
from tqdm import tqdm
import numpy as np
from rasterio.crs import CRS
import matplotlib.pyplot as plt
from collections import namedtuple
import psutil
from itertools import product
from scripts import utils
from scripts import read


class raster_preprocess:
    def __init__(
        self,
        raster_files,
        raster_crss,
        raster_multiplication,
        time_format=None,
        time_axis_name="time",
        desired_res=20,
        destination_crs=28992,
        bbox=[],
    ):
        self.files = raster_files
        self.source_crs = ["EPSG:" + c for c in raster_crss]
        self.multiplication = raster_multiplication
        self.resolution = desired_res
        self.destination_crs = "EPSG:" + str(destination_crs)
        self.bbox = bbox

        self.d_arrays = []
        self.d_arrays_used = []
        self.da = None
        self.mosaic_data = None
        self.latest_data = None

        self.tformat = time_format
        self.tname = time_axis_name

    #################### Properties ####################
    @property
    def filenames(self):
        return [rast.stem for rast in self.files]

    @property
    def timesteps(self):
        return len(self.raster_files)

    @property
    def raster_files(self):
        raster_files_good = []
        for raster in self.files:
            if raster.is_dir():
                if not raster.joinpath("w001001.adf").is_file():
                    raise FileNotFoundError(
                        "ESRI raster format requires the file: w001001.adf"
                    )
                raster_files_good.append(raster.joinpath("w001001.adf"))
            else:
                if not raster.is_file():
                    raise FileNotFoundError("Raster file not found")
                raster_files_good.append(raster)

        return raster_files_good

    @property
    def dates(self):
        return self.times[0]

    @property
    def dt(self):
        return self.times[1]

    @property
    def dt_names(self):
        return self.times[2]

    @property
    def times(self):
        # Todo automatically reorder list of d_arrays for difference func
        if len(self.raster_files) == 0 or self.tformat == None:
            return (None, None, None)

        dates = [
            utils.find_date_in_filename_from_format(raster, self.tformat)
            for i, raster in enumerate(self.files)
            if i in self.d_arrays_used or self.d_arrays_used == []
        ]
        timestamps = [d.strftime(self.tformat) for d in dates]

        dt = np.zeros(len(dates) - 1)
        names = []
        for i in range(len(dt)):
            td = dates[i + 1] - dates[i]
            dt[i] = td.days
            names.append(timestamps[i + 1] + "-" + timestamps[i])

        return (dates, dt, names, timestamps)

    @property
    def xy(self):
        xytup = namedtuple("xy_coords", ["x", "y"])
        if len(self.bbox) == 4:
            minx = self.bbox[0]
            maxx = self.bbox[1]
            miny = self.bbox[2]
            maxy = self.bbox[3]

            xy = xytup(
                np.arange(minx, maxx, self.resolution),
                np.arange(maxy, miny, -self.resolution),
            )
        else:
            xy = xytup(
                np.arange(0, self.resolution, self.resolution),
                np.arange(self.resolution, 0, -self.resolution),
            )

        return xy

    @property
    def autochunk_2d(self):
        total_memory = psutil.virtual_memory().total
        max_cells = np.round(total_memory / 300)
        xy_chunks = np.int32(np.ceil(np.sqrt(max_cells)))
        return (1, xy_chunks, xy_chunks)

    @property
    def autochunk_3d(self):
        total_memory = psutil.virtual_memory().total
        t_chunks = len(self.filenames)
        max_cells = np.round(total_memory / 300)
        xy_chunks = np.int32(np.ceil(np.sqrt(max_cells / t_chunks)))
        return (t_chunks, xy_chunks, xy_chunks)

    #################### I/O ####################
    def load(self, minfrac=0, progressbar=None, chunk=False):
        self.d_arrays = []

        if len(self.bbox) == 4:
            self.d_arrays_used = []
            self.resample_done = True

            for i, raster_file in enumerate(self.raster_files):
                # Read array
                if raster_file.suffix in (".xyz", ".txt", ".csv", ".pts"):
                    array = read.read_xyz(raster_file)
                else:
                    array = read.read_raster(raster_file)

                # Clip array
                array = array.sel(
                    x=slice(self.bbox[0], self.bbox[1]),
                    y=slice(self.bbox[3], self.bbox[2]),
                )

                # Reproject array
                if self.source_crs[i] != self.destination_crs:
                    array = imod.prepare.reproject(
                        array, src_crs=self.source_crs[i], dst_crs=self.destination_crs
                    )
                array.attrs["crs"] = self.destination_crs

                # Resample array based on desired cell size
                array = array.reindex(
                    x=self.xy.x,
                    y=self.xy.y,
                    tolerance=self.resolution,
                    method="nearest",
                )

                # Chunk array is chunk = True
                if chunk:
                    array = array.chunk(chunks=(1, "auto", "auto"))

                if isinstance(array.nodatavals, (float, int)):
                    array = array.where(array != array.nodatavals)
                else:
                    array = array.where(array != array.nodatavals[0])

                try:
                    array = array.drop_vars("band")
                except ValueError:
                    pass
                array = utils.set_da_attributes(array)
                array *= self.multiplication[i]

                if not progressbar.WasCancelled():
                    progressbar.Update(i, newmsg=f"Loading {self.filenames[i]}")
                else:
                    break

                if minfrac > 0:
                    self.d_arrays, self.d_arrays_used = self.check_data(
                        array, self.d_arrays, self.d_arrays_used, i, minfrac
                    )
                else:
                    self.d_arrays.append(array)
                    self.d_arrays_used.append(i)

        else:
            for i, raster_file in tqdm(
                enumerate(self.raster_files), desc="Loading raw raster data"
            ):
                array = xr.open_rasterio(raster_file)

                if self.source_crs[i] != self.destination_crs:
                    array = imod.prepare.reproject(
                        array, src_crs=self.source_crs[i], dst_crs=self.destination_crs
                    )
                array.attrs["crs"] = self.destination_crs
                self.d_arrays.append(array.squeeze())
                self.d_arrays_used.append(i)

                if not progressbar.WasCancelled():
                    progressbar.Update(i, newmsg=f"Loading {raster_file.stem}")
                else:
                    break

    def timeseries_to_da(self, rechunk=False):
        if self.times == (None, None, None):
            raise ValueError(
                "Could not concatenate rasters because no timestamps are defined"
            )
        da = xr.concat(self.d_arrays, dim=self.tname).assign_coords(
            coords={self.tname: self.dates}
        )
        da.attrs["descriptions"] = "combined_da_of_raster_data"

        if rechunk:
            da = da.chunk(chunks=("auto", "auto", "auto"))

        self.da = da.sortby(self.tname)

    def timeseries_to_nc(self, file):
        if self.da is None:
            self.timeseries_to_da()
        self.da.to_netcdf(file)

    def mosaic_to_tif(self, mosaic_file=None, latest_file=None):
        for data, file in zip(
            [self.mosaic_data, self.latest_data], [mosaic_file, latest_file]
        ):
            if data is not None and file is not None:
                imod.rasterio.write(file, data)

    def export_for_analysis(self, folder):
        if self.da is None:
            self.timeseries_to_da()

        y_chunk_idxs = np.cumsum(np.array(self.da.chunks[1]))
        x_chunk_idxs = np.cumsum(np.array(self.da.chunks[2]))

        if len(self.da.chunks[1]) == 1 or len(self.da.chunks[2]) == 1:
            self.da.to_netcdf(Path(folder).joinpath("chunk_1.nc"))
        else:
            for i, (xi, yi) in enumerate(product(y_chunk_idxs, x_chunk_idxs)):
                xrng = np.arange(xi - xi, xi)
                yrng = np.arange(yi - yi, yi)

                da_chunk = self.da.isel(y=yrng, x=xrng)

                da_chunk.to_netcdf(Path(folder).joinpath(f"chunk_{i}.nc"))

    #################### Processes ####################
    def combine(self):
        datasets = [arr.to_dataset(name="arr") for arr in self.d_arrays]
        self.mosaic_data = xr.combine_by_coords(datasets, combine_attrs="drop")["arr"]
        self.latest_data = None

    def mosaic(self, progressbar=None):
        new_arrays = self.d_arrays
        minx = new_arrays[0].x.min().values
        maxx = new_arrays[0].x.max().values
        miny = new_arrays[0].y.min().values
        maxy = new_arrays[0].y.max().values

        # Combine arrays to mosaic
        newarr = np.full_like(new_arrays[0].values, np.nan)
        newarr_idx = np.full_like(new_arrays[0].values, np.nan)
        progressbar.Update(0)
        for i, ar in enumerate(new_arrays):
            newarr[np.isfinite(ar.values)] = ar.values[np.isfinite(ar.values)]
            newarr_idx[np.isfinite(ar.values)] = i + 1

            if not progressbar.WasCancelled():
                progressbar.Update(i, newmsg=f"Processing {self.filenames[i]}")
            else:
                break

        if not progressbar.WasCancelled():
            newarr = newarr.squeeze()
            newarr_idx = newarr_idx.squeeze()

            self.mosaic_data = xr.DataArray(
                newarr,
                coords={
                    "x": np.arange(minx, maxx + self.resolution, self.resolution),
                    "y": np.arange(maxy, miny - self.resolution, -self.resolution),
                },
                dims=["y", "x"],
            )

            self.latest_data = xr.DataArray(
                newarr_idx,
                coords={
                    "x": np.arange(minx, maxx + self.resolution, self.resolution),
                    "y": np.arange(maxy, miny - self.resolution, -self.resolution),
                },
                dims=["y", "x"],
            )

    @staticmethod
    def check_data(array, array_list, used_list, i, minfrac):
        num_nans = xr.ufuncs.isnan(array).sum()
        total_cells = len(array.x) * len(array.y)

        if num_nans < total_cells * (1 - minfrac):
            array_list.append(array)
            used_list.append(i)
        return (array_list, used_list)
