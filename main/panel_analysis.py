import wx

# Local imports
# from validators import CharValidator, IntegerValidator, FloatRangeValidator


class AnalysisPanel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent=parent)

        # Add trend analysis
        self.btn_trend_analysis = wx.Button(parent=self, label="Trend")
        self.Bind(wx.EVT_BUTTON, self.OnTrendAnalysis, self.btn_trend_analysis)

        # Object arrangement
        self.CreateLayout()

    def CreateLayout(self):
        self.list_buttons_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.list_buttons_sizer.Add(
            self.btn_trend_analysis, 0, flag=wx.LEFT | wx.ALIGN_CENTER
        )

    def OnTrendAnalysis(self, e):
        self.preprocess.da
